# Intern Test

Thuật toán cộng 2 số lớn (được biểu diễn dưới dạng chuỗi) cho các học sinh Tiểu học

Ví dụ chạy thử
```go
func main() {
    elementarySum("165832", "2456", false); // false as for not in testing mode (with logging) 
}
```
Output (logging)
```
165832 + 2456

Bước 1: 2 + 6 = 8
Tổng = 8

Bước 2: 3 + 5 = 8
Tổng = 88

Bước 3: 8 + 4 = 12
Tổng = 288 (Nhớ 1)

Bước 4: 5 + 2 = 7
Tổng = 8288

Bước 5: 6 + 0 = 6
Tổng = 68288

Bước 6: 1 + 0 = 1
Tổng = 168288
```

